import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AstudioComponent } from './astudio/astudio.component';


const rots: Routes = [
  { path: 'feature', component: AstudioComponent },

  { path: '', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    AstudioComponent ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(rots),
    ],

    providers: [],
  bootstrap: [AppComponent, AstudioComponent]
})
export class AppModule { }
