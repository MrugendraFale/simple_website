import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AstudioComponent } from './astudio.component';

describe('AstudioComponent', () => {
  let component: AstudioComponent;
  let fixture: ComponentFixture<AstudioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AstudioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AstudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
